package blockchain

type Chain struct {
	Blocks []*Block
}

func NewChain() *Chain {
	return &Chain{[]*Block{NewGenesis()}}
}

// TODO: implement validation of block
func (chain *Chain) IsBlockValid(currentBlock, previousBlock Block) bool {
	if previousBlock.Id+1 != currentBlock.Id {
		return false
	}

	if previousBlock.Hash != currentBlock.PrevHash {
		return false
	}

	if currentBlock.CalculateHash() != currentBlock.Hash {
		return false
	}

	return true
}

func (chain *Chain) AddBlock(data string) *Block {
	prevBlock := chain.Blocks[len(chain.Blocks)-1]
	newBlock := NewBlock(data, prevBlock)
	chain.Blocks = append(chain.Blocks, newBlock)
	return newBlock
}

func NewGenesis() *Block {
	return NewBlock("Our Genesis!", &Block{})
}
