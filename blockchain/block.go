package blockchain

import (
	"crypto/sha256"
	"strconv"
	"strings"
	"time"
)

type Block struct {
	Id        int
	TimeStamp string
	Data      string
	Hash      string
	PrevHash  string
}

func NewBlock(data string, prevBlock *Block) *Block {
	timeStamp := time.Now().UTC().Format(time.RFC3339Nano)
	block := &Block{
		Id:        prevBlock.Id + 1,
		TimeStamp: timeStamp,
		Data:      data,
		Hash:      "",
		PrevHash:  prevBlock.Hash,
	}
	block.Hash = CalculateHash(block)
	return block
}

func CalculateHash(block *Block) string {
	joinedInfo := []string{strconv.Itoa(block.Id), block.TimeStamp, block.Data, block.PrevHash}
	information := strings.Join(joinedInfo, "")
	hashedInfo := sha256.Sum256([]byte(information))
	return string(hashedInfo[:])
}

func (block *Block) CalculateHash() string {
	joinedInfo := []string{strconv.Itoa(block.Id), block.TimeStamp, block.Data, block.PrevHash}
	information := strings.Join(joinedInfo, "")
	hashedInfo := sha256.Sum256([]byte(information))
	newHash := string(hashedInfo[:])
	block.Hash = newHash
	return newHash
}
