# GoVotee

## Description
An e-voting system/app build on a blockchain.

## Features

| Feature       | Technology    | Description   |
| :---:         | :---:         | :---:         |
| Blockchain    | Golang        |       -       |
| Server        |               |       -       |
| Database      |               |       -       |

## Roadmap
- [x] Create basic blockchain
- [x] Server / Client (gorilla-mux, godotenv)
- [ ] P2P (go-libp2p)
- [ ] Documentation (continuous)
- [ ] Tests (continuous)
- [ ] Data Persistence (json, sql, etc)
- [ ] Consensus (POW, POS, Practical Byzantine Fault Tolerance, Raft, etc)
- [ ] Anonymity (voters anonymity)
- [ ] Identity Verification
- [ ] Encryption (_asymmetric_, symmetric, etc)
- [ ] Integration (e.g. LDAP)
- [ ] GUI (App, Web, etc)
- [ ] Wiki documentation

## Installation
N/A

## Usage
### Tests
* go run main.go
Browser: localhost:8080
* curl localhost:8080 -d '{"vote": "<Name>"}'
Browser: localhost:8080